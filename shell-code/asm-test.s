.global _start

.text
_start:
        # Zero out '%eax'.
        xor %eax,%eax

        # Push '%eax' to create a null pointer or null byte at the end
        # of a string.
        push %eax

        # Push four bytes (they get reversed).
        pushl $0xaabbccdd

        # Zero out the kth byte of '%exp' (for fixing strings that
        # don't have multiple of four length assumes '%eax' is already
        # zeroed.
        andb %al,1(%esp)
        andb %al,2(%esp)
        andb %al,3(%esp)
        # Push two bytes (they also get reversed, and this ruins the
	      # alignment ...)
        pushw $0xeeff
        # There is no 'pushb', so this approach won't work anyway.

        # Set 'filename' to string set up at '%esp'.
        mov    %esp,%ebx

        # Set 'envp' to '{ NULL }'.
        push   %eax
        mov    %esp,%edx

        # Set 'argv' to '{ filename , arg1 , ... , argN, NULL }'.
        push   %ebx
        mov    %esp,%ecx

        # Call 'execve'
        mov    $11,%al
        int    $0x80

        # Just curious: the order of '%ebx' and '%ecx' seems backwards
        # in the corresponding binary.
        mov %esp,%eax
