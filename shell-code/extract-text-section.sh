#!/bin/bash

# Extract the .text section from a binary as binary.
#
# There are many tools for extracting a formatted version of the .text
# section, e.g. 'readelf -x .text' for a hex dump, and 'objdump -d -j
# .text -M att' for a dissassembly.  However, for a shell code we just
# want the raw binary.  With 'readelf --sections' we can get the
# offset and length of each section, and so can locate .text in
# particular.

if [[ $# != 1 ]]; then
  echo "usage: $0 ELF_BINARY" > /dev/stderr
  exit 2
fi

elf="$1"
# The 'readelf --sections' output looks like:
#
#  ...
#  [Nr] Name              Type            Addr     Off    Size   ES Flg Lk Inf Al
#  [ 0]                   NULL            00000000 000000 000000 00      0   0  0
#  [ 1] .text             PROGBITS        00000000 000034 000033 00  AX  0   0  4
#  ...
#
# The offset and size are in hex.
text="$(readelf --sections "$elf" | fgrep .text)"
offset="$(echo "$text" | awk '{ print strtonum("0x"$6) }')"
size="$(echo "$text" | awk '{ print strtonum("0x"$7) }')"
tail -c+$((offset + 1)) "$elf" | head -c$((size))
