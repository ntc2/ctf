#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv){
  if (argc != 2) {
    fprintf(stderr, "usage: %s SHELL_CODE\nargc = %i\n", argv[0], argc);
    exit(2);
  }

  return ((int(*)()) argv[1])();
}
