#!/bin/bash

# Disassemble a stream of bytes.

# E.g., dissemble the output of ./extract-text-section.sh to see that
# it worked.
if [[ $# != 0 ]]; then
  echo "usage: BINARY_STREAM | $0" > /dev/stderr
  exit 2
fi
x86dis -e 0 -s att
