/*
 * Print the hex value of the stack pointer of main in this program.
 *
 * Assumes 64 bit architecture.
 *
 * Copied from my buffer-overflows repo on GitHub.
 */
#include <stdio.h>

int main () {
    unsigned long int rbp;
    __asm__("movq %%rbp, %0" : "=r"(rbp));
    printf("0x%016lx\n", rbp);
    return 0;
}
