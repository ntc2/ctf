#!/bin/bash

# Generate and print shell code for given command.

asm="$(mktemp)"
obj="$(mktemp)"
bin="$(mktemp)"
./generate-shell-code.asm.py "$@" > "$asm"
as "$asm" -o "$obj"
./extract-text-section.sh "$obj" > "$bin"
# The '-P' means "use Perl regex mode", which can handle raw hex in
# patterns.
if grep -qP '\x00' < "$bin"; then
  echo 'The extracted binary contains null (\0), careful!' > /dev/stderr
fi
if grep -qP '%' < "$bin"; then
  echo 'The extracted binary contains percent (%), beware of printf()!' > /dev/stderr
fi
cat "$bin"
