#!/usr/bin/expect -f
# Based on http://linuxaria.com/howto/2-practical-examples-of-expect-on-the-linux-cli

# Usage: $0 PASSWORD COMMAND
#
# To run COMMAND, answering a password prompt with PASSWORD.

set timeout 10
set pass [lrange $argv 0 0]
set cmd  [lrange $argv 1 end]
# To use only some args use e.g. '[lrange $argv 2 end]', which
# apparently is TCL code.
spawn bash -c "$cmd"
expect "*password:*"
send -- "$pass\r"
interact
