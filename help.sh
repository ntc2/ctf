#!/bin/bash

######################################################################

function help:usage {
    echo "Error: $1

Usage: $0 [expect] (push | pull | ssh | nuke) ARGS

Add 'expect' to avoid typing the password.  Any extra ARGS are passed
to the subcommand.

Example: use

  $0 push --delete

to make the remote repo match the local repo exactly. Use

  $0 expect push --delete

to do the same without having to enter a password.

There must be a file help.config in the root of the Git repo
containing $PWD defining the 'WARGAME_*' variables."

    exit 2
}

######################################################################

function help:gitdir {
  git rev-parse --show-toplevel || help:usage "not in a Git dir."
}

######################################################################
# Push or pull the Git repo that ./help.sh is run from -- not
# necessarily where ./help.sh itself is stored. The repo name is
# inferred.

function help:push {
  rsync -avz --human-readable --update "$@" \
    "$(help:gitdir)" \
    "$user"@"$host":"$dir"
}

# The trailing slash on the source is important here: it means to copy
# the contents of the source into the destination. Without the
# trailing slash, the source itself is copied.
function help:pull {
  rsync -avz --human-readable --update "$@" \
    "$user"@"$host":"$dir"/"$(basename "$(help:gitdir)")"/ \
    "$(help:gitdir)"
}

######################################################################

# SSH into narnia. Useful because they don't allow screen and they
# kick you after a timeout :P
#
# The 'cd ... ; exec bash' is a hack to cd but stay logged in. I would
# expect a more direct way to do this ...
function help:ssh {
  ssh -t "$user"@"$host" "$@" \
    "cd \"$dir/$(basename $(help:gitdir))\"; exec bash"
}

# Delete our files on the game machine.  Useful after finishing a
# level.  Do this before setting the new user and pass in help.config!
function help:nuke {
  ssh -t "$user"@"$host" "$@" \
    "rm -rf \"$dir\""

}

######################################################################

# Recurse!
function help:expect {
  "$(dirname "$0")"/expect.sh "$pass" "$(dirname "$0")"/help.sh "$@"
}

######################################################################

# Put a help.config in the root of your Git repo which sets these
# 'WARGAME_*' variables.
source "$(help:gitdir)"/help.config \
  || help:usage "could not load $(help:gitdir)/help.config."
user="$WARGAME_USER"
host="$WARGAME_HOST"
pass="$WARGAME_PASS"
# Full path to directory in which to mirror repo on $host.  E.g., for
# a local repo named narnia.git, it will be mirrored to
# $repo/narnia.git.
dir="$WARGAME_DIR"

# Extra args are passed to the subcommand.
cmd="$1"; shift
case "$cmd" in
  expect | push | pull | ssh | nuke) help:$cmd "$@" ;;
  *) help:usage "bad command: $cmd" ;;
esac
