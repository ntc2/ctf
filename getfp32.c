/*
 * Print the hex value of the stack pointer of main in this program.
 *
 * Assumes 32 bit architecture.
 *
 * Copied from my buffer-overflows repo on GitHub.
 */
#include <stdio.h>

int main () {
    unsigned int ebp;
    __asm__("movl %%ebp, %0" : "=r"(ebp));
    printf("0x%08x\n", ebp);
    return 0;
}
