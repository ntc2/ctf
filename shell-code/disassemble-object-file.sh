#!/bin/bash

# E.g., to disassemble the output of 'as'.

if [[ $# != 1 ]]; then
  echo "usage: $0 OBJECT_FILE" > /dev/stderr
  exit 2
fi
objdump -j .text -M att -d "$1"
